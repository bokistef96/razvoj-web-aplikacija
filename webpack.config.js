const path = require('path');

module.exports = {
    entry: {
        app: ['./src/index.js'] //za prva.js
    },
    output: {
        filename: "main.js",
        path: path.resolve(__dirname, 'dist')
    },
    devtool: "source-map",
    module: {
        rules: [{
            //kako ce se obraditi fajlovi
            test: /\.js$/, //regularni izraz $-ako se string zavrsava sa tom reci
            exclude: /(node_modules|bower_components)/, //da ne trazi node_modules jer ima mng fajlova
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['es2016']
                }
            }
        }]
    }
}