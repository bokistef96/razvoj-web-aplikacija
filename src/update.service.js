import {interval } from 'rxjs/observable/interval';
import * as Rxjs from 'rxjs';
import { Student } from "./student";
import { Studenti } from "./studenti";
import { StudentsService } from "./students.service";

export class UpdateService {

    static html(nizStudenata){     
        
                let bodyPart=document.getElementById("bodyPart");
                let portlets=document.getElementById("portletsArray");
                let other=document.getElementById("other");
        
                let layout="<label><b>Uneti broj indexa studenta za izmenu:</b></label>"+
                "<input id='pretrazi' type='text' class='form-control' placeholder='Broj indexa'>";
                other.innerHTML=layout;
        
                let pretrazi=document.getElementById("pretrazi");
        
                Rxjs.Observable.fromEvent(pretrazi, "input")
                .map(ev => ev.target.value)
                .map(str => UpdateService.findSugestions(nizStudenata,str))
                .debounceTime(300)
                .subscribe(parametar => {
                    let portlets=document.getElementById("portletsArray");
                    portlets.innerHTML="";
                    UpdateService.htmlUpdate(parametar);
                    
                });

                

                
        
            }
        
            static findSugestions(nizStudenata,str) {
                let pom=[]; 
                if(str!=="")    
                {
                  pom = nizStudenata.filter(element => element.brIndexa.includes(str));
                }
                else pom=[];
                return pom;
               
            }
            static htmlUpdate(nizStudenata){     
                let portlets=document.getElementById("portletsArray");
                portlets.innerHTML="";
        
                interval(300)
                .take(1)
                .subscribe(() => {
                   
                    nizStudenata.forEach(student=>{
                        portlets.innerHTML+=student.crtaj();

                        interval(300)
                        .take(1)
                        .subscribe(() => {
                            let divDgume=document.getElementById(`dugmeDiv${student.id}`);                           
                            let dugme=document.createElement("button");
                            divDgume.appendChild(dugme);
                
                            dugme.innerHTML="Izmeni studenta";
                            dugme.className="btn btn-sm ";
                            dugme.id=`dugme${student.id}`;
                         

                            let dugmeClick=document.getElementById(`dugme${student.id}`);
                            Rxjs.Observable.fromEvent(dugmeClick,"click")
                            .subscribe(()=>
                                {   
                                    UpdateService.update(student);

                                    
                                    let title=document.getElementById("nazivModal");
                                    title.innerHTML="Izmena";
                                    let body=document.getElementById("bodyModal");
                                    body.innerHTML="Uspesno ste izmenili traženog studenta";
                                    portlets.innerHTML="";

                                    
                            })
                        
                        });

                   });
                 
                });
                
                   
                }


                
        static update(student){

            let other=document.getElementById("other");
            let updateForm="<label><b>Uneti podatke za ažuriranje traženog studenta: </label>"+
            " <input id='imeU' type='text' class='inputUpdate form-control ' placeholder='Ime'>"+
            "<input id='prezimeU' type='text' class='form-control inputUpdate' placeholder='Prezime'>"+
            "<input id='brIndexaU' type='text' class='form-control inputUpdate' placeholder='Broj indexa'>"+
            "<input id='brIspitaU' type='text' class='form-control inputUpdate' placeholder='Broj ispita'>"+
            "<button class='btn btn-sm inputUpdate' id='potvrdiIzmenu'>Izmeni</button>";
            other.innerHTML=updateForm;

            interval(300)
            .take(1)
            .subscribe(() => {
                document.getElementById("imeU").value=student.ime,
                document.getElementById("prezimeU").value=student.prezime,
                document.getElementById("brIndexaU").value=student.brIndexa,
                document.getElementById("brIspitaU").value=student.brPolozenihIspita;
                let dugmeIzmena=document.getElementById("potvrdiIzmenu");
                dugmeIzmena.setAttribute("data-target", "#myModal");
                dugmeIzmena.setAttribute("data-toggle", "modal");
                Rxjs.Observable.fromEvent(dugmeIzmena,"click")
                .subscribe(()=>
                    {   
                        StudentsService.updateConfirm(student.id);
                })
            });
    
        }

    }
