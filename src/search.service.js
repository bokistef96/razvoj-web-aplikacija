import {interval } from 'rxjs/observable/interval';
import * as Rxjs from 'rxjs';
import { Student } from "./student";
import { Studenti } from "./studenti";
import { StudentsService } from "./students.service";



export class SearchService {

    static html(nizStudenata){     

        let bodyPart=document.getElementById("bodyPart");
        let portlets=document.getElementById("portletsArray");
        let other=document.getElementById("other");

        let layout="<label><b>Uneti broj indexa studenta za pretraživanje:</b></label>"+
        "<input id='pretrazi' type='text' class='form-control' placeholder='Broj indexa'>";
        other.innerHTML=layout;

        let pretrazi=document.getElementById("pretrazi");

        Rxjs.Observable.fromEvent(pretrazi, "input")
        .map(ev => ev.target.value)
        .map(str => SearchService.findSugestions(nizStudenata,str))
        .debounceTime(400)
        .subscribe(parametar => {

            SearchService.htmlShow(parametar);

        });
        

    }

    static findSugestions(nizStudenata,str) {
        let pom=[]; 
        if(str!=="")    
        {
          pom = nizStudenata.filter(element => element.brIndexa.includes(str));
        }
        else pom=[];
        return pom;
       
    }
    static htmlShow(nizStudenata){     
       let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";

    

        interval(700)
        .take(1)
        .subscribe(() => {
           
            nizStudenata.forEach(student=>{
                 portlets.innerHTML+=student.crtaj();
                });
            
            
        });
    

    }




}