import * as Rxjs from 'rxjs';
import { Student } from "./student";
import {interval } from 'rxjs/observable/interval';
import { Studenti } from "./studenti";
import { UpdateService } from "./update.service";
import { AddService } from "./add.service";

export class StudentsService {
    
        static get() { //static- pripada klasi a ne objektu
            return fetch("http://localhost:3000/students/")
                .then(response => response.json())
        }

        static add(){ 

            let url="http://localhost:3000/students/";

            let addStudent=Rxjs.Observable.fromPromise(
                fetch(`${url}`, {
                    method: 'post',
                    headers: {
                        
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                   
                    body: JSON.stringify({
                        ime: document.getElementById("imeA").value,
                        prezime: document.getElementById("prezimeA").value,
                        brIndexa: document.getElementById("brIndexaA").value,
                        brPolozenihIspita:document.getElementById("brIspitaA").value
                    })
                  })
                .then(response => {
            
                    let StudentiN=new Studenti();
                    StudentiN.osveziNiz();
                    let nizStudenata=StudentiN.studentiNiz;

                    AddService.html();

                   

                   
                  })
                )
               
        }

        static updateConfirm(id){
            
            let url="http://localhost:3000/students/";

            let put=Rxjs.Observable.fromPromise(
                fetch(`${url}${id}`, {
                    method: 'put',
                    headers: {
                        
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                   
                    body: JSON.stringify({
                        ime: document.getElementById("imeU").value,
                        prezime: document.getElementById("prezimeU").value,
                        brIndexa: document.getElementById("brIndexaU").value,
                        brPolozenihIspita:document.getElementById("brIspitaU").value
                    })
                  })
                .then(response => {
                    
                    UpdateService.html(nizStudenata);
        
                  })
                )
        }


        static delete(id){ 

            let url="http://localhost:3000/students/";

            let deleteStudent=Rxjs.Observable.fromPromise(
                fetch(`${url}${id}`, {
                    method: 'delete',
                    headers: {
                        
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                  })
                .then(response => {

                    let StudentiN=new Studenti();
                    StudentiN.osveziNiz();
                    
                  })
                )
                    
        } 
    
    }