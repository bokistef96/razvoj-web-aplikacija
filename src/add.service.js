import {interval } from 'rxjs/observable/interval';
import * as Rxjs from 'rxjs';
import { Student } from "./student";
import { Studenti } from "./studenti";
import { StudentsService } from "./students.service";

export class AddService {

    static html(){ 

       
        let other=document.getElementById("other");
       // other.innerHTML='';

       
        let layout="<div id='addF'><label><b>Uneti podatke za dodavanje novog studenta:</b> </label>"+
        "<input id='imeA' type='text' class='form-control inputUpdate' placeholder='Ime'>"+
        "<input id='prezimeA' type='text' class='form-control inputUpdate' placeholder='Prezime'>"+
        "<input id='brIndexaA' type='text' class='form-control inputUpdate' placeholder='Broj indexa'>"+
        "<input id='brIspitaA' type='text' class='form-control inputUpdate' placeholder='Broj ispita'></div>"
        ;
        other.innerHTML=layout;

        interval(50)
        .take(1)
        .subscribe(() => {
                            
            let dugme=document.createElement("button");
            let addForm=document.getElementById("addF");
            addForm.appendChild(dugme);

            dugme.innerHTML="Dodaj studenta";
            dugme.className="btn btn-md inputUpdate ";
            dugme.id="dugmeAdd";
            dugme.setAttribute("data-target", "#myModal");
            dugme.setAttribute("data-toggle", "modal");

            let dugmeAdd=document.getElementById("dugmeAdd");
            Rxjs.Observable.fromEvent(dugmeAdd,"click")
            .subscribe(()=>
                {   
                    StudentsService.add();

                    let title=document.getElementById("nazivModal");
                    title.innerHTML="Dodavanje studenta";
                    let body=document.getElementById("bodyModal");
                    body.innerHTML="Uspesno ste dodali novog studenta";

                    
            })
        
        });

       

    }
}