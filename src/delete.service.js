import {interval } from 'rxjs/observable/interval';
import * as Rxjs from 'rxjs';
import { Student } from "./student";
import { Studenti } from "./studenti";
import { StudentsService } from "./students.service";

export class DeleteService {
    static html(){     
        
                let StudentiN=new Studenti();
                StudentiN.osveziNiz();
                let nizStudenata=StudentiN.studentiNiz;

                let bodyPart=document.getElementById("bodyPart");
                let portlets=document.getElementById("portletsArray");
                let other=document.getElementById("other");
        
                let layout="<label><b>Uneti broj indexa studenta za brisanje:</b></label>"+
                "<input id='pretrazi' type='text' class='form-control' placeholder='Broj indexa'>";
                other.innerHTML=layout;
        
                let pretrazi=document.getElementById("pretrazi");
        
                Rxjs.Observable.fromEvent(pretrazi, "input")
                .map(ev => ev.target.value)
                .map(str => DeleteService.findSugestions(nizStudenata,str))
                .debounceTime(300)
                .subscribe(parametar => {
                    let portlets=document.getElementById("portletsArray");
                    portlets.innerHTML="";
                    DeleteService.htmlDelete(parametar);
               
                    
                });
              
        
            }
        
            static findSugestions(nizStudenata,str) {
                let pom=[]; 
                if(str!=="")    
                {
                  pom = nizStudenata.filter(element => element.brIndexa.includes(str));
                }
                else pom=[];
                return pom;
               
            }
            static htmlDelete(nizStudenata){     
                let portlets=document.getElementById("portletsArray");
                portlets.innerHTML="";
        
                interval(300)
                .take(1)
                .subscribe(() => {
                   
                    nizStudenata.forEach(student=>{
                        portlets.innerHTML+=student.crtaj();
                        interval(300)
                        .take(1)
                        .subscribe(() => {
                            let divDgume=document.getElementById(`dugmeDiv${student.id}`);                           
                            let dugme=document.createElement("button");
                            divDgume.appendChild(dugme);
                
                            dugme.innerHTML="Obriši studenta";
                            dugme.className="btn btn-sm ";
                            dugme.id=`dugme${student.id}`;
                            dugme.setAttribute("data-target", "#myModal");
                            dugme.setAttribute("data-toggle", "modal");

                            let dugmeClick=document.getElementById(`dugme${student.id}`);
                            Rxjs.Observable.fromEvent(dugmeClick,"click")
                            .subscribe(()=>
                                {   
                                    StudentsService.delete(student.id);

                                    let title=document.getElementById("nazivModal");
                                    title.innerHTML="Brisanje studenta";
                                    let body=document.getElementById("bodyModal");
                                    body.innerHTML="Uspesno ste obrisali traženog studenta";
                                    portlets.innerHTML="";
                                    
                                    let StudentiN=new Studenti();
                                    StudentiN.osveziNiz();
                                    DeleteService.html();

                                    
                            })
                        
                        });

                   });
                 
                });
                
                   
                }

    }
