import * as Rxjs from 'rxjs';
import { Student } from "./student";
import { Studenti } from "./studenti";
import { StudentsService } from "./students.service";
import { debounceTime } from 'rxjs/operators/debounceTime';
import {interval } from 'rxjs/observable/interval';
import { SearchService } from "./search.service";
import { DeleteService } from "./delete.service";
import { UpdateService } from "./update.service";
import { AddService } from "./add.service";

let bodyPart=document.getElementById("bodyPart");

let btnPocetna=document.getElementById("btnPocetna");
let btnPretrazi=document.getElementById("btnPretrazi");
let btnIzmeni=document.getElementById("btnIzmeni");
let btnDodaj=document.getElementById("btnDodaj");
let btnObrisi=document.getElementById("btnObrisi");

Rxjs.Observable.fromEvent(btnPretrazi,"click")
.subscribe(()=>
    {  
        let StudentiN=new Studenti();
        StudentiN.osveziNiz();
        let nizStudenata=StudentiN.studentiNiz;

        let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";

        btnPocetna.className="btn btn-lg btn-block disabled selection";
        btnPretrazi.className="btn btn-lg btn-block active selection";
        btnDodaj.className="btn btn-lg btn-block disabled selection";
        btnObrisi.className="btn btn-lg btn-block disabled selection";
        btnIzmeni.className="btn btn-lg btn-block disabled selection";

        SearchService.html(nizStudenata);
    
})

Rxjs.Observable.fromEvent(btnDodaj,"click")
.subscribe(()=>
    {  

        let StudentiN=new Studenti();
        StudentiN.osveziNiz();
        let nizStudenata=StudentiN.studentiNiz;

        let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";
        
        btnPocetna.className="btn btn-lg btn-block disabled selection";
        btnPretrazi.className="btn btn-lg btn-block disabled selection";
        btnDodaj.className="btn btn-lg btn-block active selection";
        btnObrisi.className="btn btn-lg btn-block disabled selection";
        btnIzmeni.className="btn btn-lg btn-block disabled selection";

        AddService.html();
    
})

Rxjs.Observable.fromEvent(btnObrisi,"click")
.subscribe(()=>
    {  
        let StudentiN=new Studenti();
        StudentiN.osveziNiz();
        let nizStudenata=StudentiN.studentiNiz;

        let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";

        btnPocetna.className="btn btn-lg btn-block disabled selection";
        btnPretrazi.className="btn btn-lg btn-block disabled selection";
        btnDodaj.className="btn btn-lg btn-block disabled selection";
        btnObrisi.className="btn btn-lg btn-block active selection";
        btnIzmeni.className="btn btn-lg btn-block disabled selection";

        DeleteService.html();
})

Rxjs.Observable.fromEvent(btnIzmeni,"click")
.subscribe(()=>
    {  
        let StudentiN=new Studenti();
        StudentiN.osveziNiz();
        let nizStudenata=StudentiN.studentiNiz;

        let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";

        btnPocetna.className="btn btn-lg btn-block disabled selection";
        btnPretrazi.className="btn btn-lg btn-block disabled selection";
        btnDodaj.className="btn btn-lg btn-block disabled selection";
        btnObrisi.className="btn btn-lg btn-block disabled selection";
        btnIzmeni.className="btn btn-lg btn-block active selection";
        

        UpdateService.html(nizStudenata);
})

Rxjs.Observable.fromEvent(btnPocetna,"click")
.subscribe(()=>
    {  
        let StudentiN=new Studenti();
        StudentiN.osveziNiz();
        let nizStudenata=StudentiN.studentiNiz;

        let portlets=document.getElementById("portletsArray");
        portlets.innerHTML="";

        btnPocetna.className="btn btn-lg btn-block active selection";
        btnPretrazi.className="btn btn-lg btn-block disabled selection";
        btnDodaj.className="btn btn-lg btn-block disabled selection";
        btnObrisi.className="btn btn-lg btn-block disabled selection";
        btnIzmeni.className="btn btn-lg btn-block disabled selection";

        let other=document.getElementById("other");
        other.innerHTML=
            "<div id='pocetna'>"
            +"<div class='naslov'> <h3> Dobro došli na sajt koji služi za pregled studenata Elektronsog fakulteta u Nišu ! </h3> </div> " 
            +"<div class='tekst'>"
            +"     <h4> Korisni linkovi povezani sa Elektronskim fakultetom:<br/> <h4>"
            +"    <ul>"
            +"    <li><a href='http://www.elfak.ni.ac.rs'>ELFAK</a>-Sajt fakulteta</li>"
            +"    <li><a href='https://sip.elfak.ni.ac.rs/'>SIP</a>-Studentski informativni portal</li>"
            +"    <li><a href='https://sicef.info'>SICEF</a>-Studentski inovacioni centar Elektronsog fakulteta</li>"
            +"    <li><a href='https://cs.elfak.ni.ac.rs/nastava/'>CS</a>-Katedra za računarstvo</li>"
            +"    </ul> "
            +"</div>"
            +"</div>";
        
    
})










































