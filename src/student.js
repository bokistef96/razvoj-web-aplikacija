import { StudentsService } from "./students.service";
import * as Rxjs from 'rxjs';
import {interval } from 'rxjs/observable/interval';

export class Student {

    constructor(brIndexa, ime, prezime, brPolozenihIspita,id) {
        this.brIndexa = brIndexa;
        this.ime = ime;
        this.prezime = prezime;
        this.brPolozenihIspita = brPolozenihIspita;
        this.id=id;
       
       }
       
    crtaj(){

        let layout=" <div class='portleta  ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'>"+
        "<div class='portlet-headera ui-widget-header ui-corner-all'>"+this.ime+" "+this.prezime+"</div>"+
        "<div class='portlet-contenta'> Broj indexa: "+ this.brIndexa +"</br> Ime: "+this.ime+" </br> Prezime: "+this.prezime+"</br> Broj polozenih ispita: "+this.brPolozenihIspita+"</div>"
      +` <div class="dugme" id="dugmeDiv${this.id}"></div>`;
        

      return layout;


    }


}